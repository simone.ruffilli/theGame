import { Field } from './Field';

function gameLoop(): void {

    /*
        So, I thought about creating an helper method in the Field object
        that executes all the functions below, however, I think it's cleaner
        to have the gameLoop here exposing the high level action sequence
    */

    field.updateAIPlayer();
    field.updateHumanPlayer();
    field.updateBall();
    field.checkCollision();
    field.drawField();

}

function keyDown(evt: KeyboardEvent): void {
    let keyCode = evt.keyCode
    switch (evt.keyCode) {
        case 38:
            field.player1.movement = { up: true, down: false };
            break;

        case 40:
            field.player1.movement = { up: false, down: true };
            break;
    }
}

function keyUp(evt: KeyboardEvent): void {
    let keyCode = evt.keyCode
    switch (evt.keyCode) {
        case 38:
            field.player1.movement = { up: false, down: false };
            break;

        case 40:
            field.player1.movement = { up: false, down: false };
            break;
    }
}

var field: Field = new Field('gc');

window.onload = function () {
    document.addEventListener('keydown', keyDown, false);
    document.addEventListener('keyup', keyUp, false);
    setInterval(gameLoop, 1000 / 60);
}
