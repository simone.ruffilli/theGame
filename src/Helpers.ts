export interface XYValue {
    x: number,
    y: number
}

export interface Rectangle {
    x1: number,
    y1: number,
    x2: number,
    y2: number
}

export function isColliding(a:Rectangle, b:Rectangle):boolean {
      return !(a.x1 > b.x2 ||
           a.x2 < b.x1 ||
           a.y1 > b.y2 ||
           a.y2 < b.y1);
}
