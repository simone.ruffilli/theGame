import {Rectangle} from './Helpers'

export interface playerMovement {
    up: boolean,
    down: boolean
}

export class Player {

    whichPlayer: number;
    height: number;
    thickness: number;
    score: number;
    speed: number;
    movement: playerMovement;
    fieldSize: {width:number, height:number}
    distanceFromBorder:number = 20;

    private _yPosition: number;
    private _xPosition: number;

    constructor(player: number, fieldSize: {width:number, height:number}) {
        this.thickness = 10;
        this.height = 100;
        this.score = 0;
        this.speed = 4;
        this.movement = { up: false, down: false };
        this.fieldSize = fieldSize;

        switch (player) {
            case 1:
                this._xPosition = this.distanceFromBorder;
                this._yPosition = (this.fieldSize.height - this.height) / 2;
                break;
            case 2:
                this._xPosition = this.fieldSize.width - this.thickness - this.distanceFromBorder;
                this._yPosition = (this.fieldSize.height - this.height) / 2;
                break;
        }
    }

    playerToUp(): void {
        if (this.higherPos > 0)
            this._yPosition -= this.speed;
    }

    playerToDown(): void {
        if (this.lowerPos < this.fieldSize.height)
            this._yPosition += this.speed;
    }

    get lowerPos(): number {
        return this._yPosition + this.height;
    }

    get centerPos(): number {
        return this._yPosition + this.height / 2;
    }

    get higherPos(): number {
        return this._yPosition;
    }

    get xPosition(): number {
        return this._xPosition;
    }

    get boundaries(): Rectangle {
        return {x1: this.xPosition, y1: this.higherPos, x2:this.xPosition+this.thickness, y2: this.lowerPos}
    }

}