export function playSound(sound: string): void {
    let soundEnabledElement = <HTMLInputElement>document.getElementById('sound-enabled');

    if (soundEnabledElement.checked == true) {
        let audio = <HTMLAudioElement>document.getElementById(sound);
        audio.play();
    }

}