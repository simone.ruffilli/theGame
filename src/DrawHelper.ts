export class DrawHelper {

    canvas: HTMLCanvasElement;
    canvasContext: CanvasRenderingContext2D;

    constructor(canvasElement: string) {
        this.canvas = <HTMLCanvasElement>document.getElementById(canvasElement);
        this.canvasContext = <CanvasRenderingContext2D>this.canvas.getContext("2d");
    }

    drawRectangle(x: number, y: number, width: number, height: number, color: string) {
        this.canvasContext.fillStyle = color;
        this.canvasContext.fillRect(x, y, width, height);
    }

    drawLine(
        x1: number, y1: number,
        x2: number, y2: number,
        lineDash: [number, number] = [0, 0],
        strokeStyle: string = 'white'): void {

        this.canvasContext.setLineDash([2, 3]);
        this.canvasContext.strokeStyle = 'white';
        this.canvasContext.beginPath();
        this.canvasContext.moveTo(this.canvas.width / 2, 0);
        this.canvasContext.lineTo(this.canvas.width / 2, this.canvas.height);
        this.canvasContext.stroke();
    }

    drawText(text:string, x: number, y:number, font:string = '20px "Press Start 2P"'):void {
        this.canvasContext.font = font;
        this.canvasContext.fillText(text, x, y);
    }
}