import { Ball } from './Ball';
import { Player } from './Player';
import { playerMovement } from './Player';
import { DrawHelper } from './DrawHelper';
import { playSound } from './sound'
import { isColliding } from './Helpers'

export class Field {
    canvas: HTMLCanvasElement;
    height: number;
    width: number;
    player1: Player;
    player2: Player;
    ball: Ball;
    private draw: DrawHelper;

    constructor(canvasElement: string) {
        this.canvas = <HTMLCanvasElement>document.getElementById(canvasElement);
        this.height = this.canvas.height;
        this.width = this.canvas.width;
        this.player1 = new Player(1, { width: this.width, height: this.height });
        this.player2 = new Player(2, { width: this.width, height: this.height });
        this.ball = new Ball();
        this.draw = new DrawHelper(canvasElement);
    }

    updateAIPlayer(): void {
        let p = this.player2;
        if (this.ball.pos.y < p.centerPos) {
            p.playerToUp();
        } else p.playerToDown();
    }

    updateHumanPlayer(): void {
        let p = this.player1;
        if (p.movement.up)
            p.playerToUp();
        if (p.movement.down)
            p.playerToDown();
    }

    updateBall(): void {
        this.ball.moveBall(this.ball.pos.x + this.ball.directionSpeed.x,
            this.ball.pos.y += this.ball.directionSpeed.y);
    }

    drawField(): void {
        let d = this.draw;
        let ball = this.ball;
        //Field
        d.drawRectangle(0, 0, this.width, this.height, "black");

        //Ball
        d.drawRectangle(ball.pos.x, ball.pos.y, ball.radius, ball.radius, "white");

        //Player 1
        d.drawRectangle(this.player1.xPosition,
            this.player1.higherPos,
            this.player1.thickness,
            this.player1.height,
            "white");

        //Player 2
        d.drawRectangle(this.player2.xPosition,
            this.player2.higherPos,
            this.player2.thickness,
            this.player2.height,
            "white");

        //Midfield line
        d.drawLine(this.canvas.width / 2, 0, this.canvas.width / 2, this.canvas.height, [2, 3]);

        //Score
        d.drawText(this.player1.score.toString(), this.canvas.width / 2 - 50 - 20, 50);
        d.drawText(this.player2.score.toString(), this.canvas.width / 2 + 50, 50);
    }

    checkCollision(): void {

        let p1: Player = this.player1;
        let p2: Player = this.player2;
        let ball: Ball = this.ball;

        //Bouncing on left paddle
        if (isColliding(p1.boundaries, ball.boundaries)) {
            ball.directionSpeed.x = -ball.directionSpeed.x;
            playSound('paddle');
        }

        //Bouncing on right paddle
        if (isColliding(p2.boundaries, ball.boundaries)) {
            ball.directionSpeed.x = -ball.directionSpeed.x;
            playSound('paddle');
        }

        //Touching left side, P2 scores
        if (ball.pos.x < 0) {
            ball.moveBall(this.width / 2, Math.random() * (this.height - ball.radius));
            p2.score++;
            playSound('score');
            console.log("p2 scores")
        }

        //Touching right side, P1 scores
        if (ball.pos.x > this.width - ball.radius) {
            ball.moveBall(this.width / 2, Math.random() * (this.height - ball.radius));
            p1.score++;
            playSound('score');
            console.log("p1 scores")
        }

        // Bouncing on top or bottom
        if (ball.pos.y + ball.radius > this.height ||
            ball.pos.y < 0) {
            ball.directionSpeed.y = -ball.directionSpeed.y;
            playSound('wall');
        }

    }
}