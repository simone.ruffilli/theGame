import { XYValue } from './Helpers';
import { Rectangle } from './Helpers'

export class Ball {
    pos: XYValue;
    directionSpeed: XYValue;
    radius: number;
    speedFactor: number;

    constructor() {
        this.pos = { x: 100, y: 100 };
        this.directionSpeed = { x: 5, y: 5 };
        this.radius = 10;
    }

    moveBall(x: number, y: number): void {
        this.pos.x = x;
        this.pos.y = y;
    }

    get boundaries(): Rectangle {
        return { x1: this.pos.x, y1: this.pos.y, x2: this.pos.x + this.radius, y2: this.pos.y + this.radius }
    }
}